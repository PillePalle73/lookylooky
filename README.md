https://docs.nvidia.com/jetson/archives/l4t-archived/l4t-3231/index.html#page/Tegra%20Linux%20Driver%20Package%20Development%20Guide/display_configuration.html

https://desertbot.io/blog/jetson-xavier-nx-headless-wifi-setup

https://www.jetsonhacks.com/2019/08/21/jetson-nano-headless-setup/

https://elinux.org/Jetson_AGX_Xavier

https://developer.ridgerun.com/wiki/index.php?title=Main_Page

https://f1tenth.readthedocs.io/en/latest/getting_started/software_setup/optional_software_nx.html

https://www.stereolabs.com/blog/getting-started-with-jetson-xavier-nx/

https://koansoftware.com/yocto-project-on-nvidia-jetson-agx-xavier/

https://www.pyimagesearch.com/2020/03/25/how-to-configure-your-nvidia-jetson-nano-for-computer-vision-and-deep-learning/

https://towardsdatascience.com/using-cv-and-ml-to-monitor-activity-while-working-from-home-f59e5302fe67

https://jkjung-avt.github.io/setting-up-xavier-nx/

https://forums.developer.nvidia.com/t/how-to-display-the-boot-menu/158205/44

https://forums.developer.nvidia.com/t/set-boot-sequence-for-xavier-nx/171186


https://www.pyimagesearch.com/category/keras-and-tensorflow/

HAT.tec
https://www.youtube.com/watch?v=vKcmQSs8NlU

ML Playlist
https://www.youtube.com/playlist?list=PLNmsVeXQZj7qoIUw0MBYQ9qJffZAVdRWC


H.265 SEI (supplemental enhancement information)

https://what-when-how.com/Tutorial/topic-397pct9eq3/High-Efficiency-Video-Coding-HEVC-55.html

https://www.researchgate.net/publication/260665391_Overview_of_HEVC_High-Level_Syntax_and_Reference_Picture_Management

https://video.stackexchange.com/questions/16958/ffmpeg-encode-in-all-i-mode-h264-and-h265-streams


"Deadly Swapper":

https://github.com/lbborkowski/wind-turbine-detector


ONNX-I/O-Names:

https://github.com/onnx/onnx/issues/2657

https://elinux.org/TensorRT/ONNX

https://github.com/daquexian/onnx-simplifier

https://stackoverflow.com/questions/61139855/how-to-convert-tensorflow-2-trained-with-keras-model-to-onnx-format

https://www.codeproject.com/Articles/5278501/Making-Keras-Models-Portable-Using-ONNX


Jetson Stuff:

https://github.com/feitgemel/face-recognition


S-BUS-Stuff:

https://www.youtube.com/watch?v=CULas2y_sXI


Geo-Stuff:

https://gis.stackexchange.com/questions/1763/seeking-free-elevation-data-for-europe


Cheat Sheets / Nachschlagen:

https://techarge.in/git-github-cheat-sheet/

https://techarge.in/python-cheatsheet/

https://makefiletutorial.com/

https://www.rosettacode.org/wiki/Rosetta_Code

https://github.com/tsoding


FPGA:

https://www.adiuvoengineering.com/blog/categories/microzed-chronicles


https://ohshitgit.com/

https://www.pyimagesearch.com/category/keras-and-tensorflow/

https://github.com/Megvii-BaseDetection/YOLOX/tree/main/demo/ONNXRuntime


https://www.pyimagesearch.com/2021/08/02/pytorch-object-detection-with-pre-trained-networks/


Ideen Celine und Rest der Rasselbande:

iperf3 zum Netzwerk-Durchmessen nehmen ?

bei Video Paket-Size auf Vielfache von 188 Bytes und unter der MRU

iperf3 -s

iperf3 -c 192.168.178.21 -l 1316

iperf3 -uZVc 192.168.178.21  -l 1316 --get-server-output -b 10M


Vorlesungen:

https://niessner.github.io/I2DL/


Nvidia-Offiziell-Benchmarks:

https://github.com/NVIDIA-AI-IOT/jetson_benchmarks

https://forums.developer.nvidia.com/t/deep-learning-inference-benchmarking-instructions/73291


Einzelgraphen für Objectness,Bounding-Boxes,...:

https://pythonrepo.com/repo/ultralytics-yolov3


Yolo und int8 & DLA:

https://github.com/jkjung-avt/tensorrt_demos#int8_and_dla

https://jkjung-avt.github.io/trt-yolo-custom-updated/

https://gilberttanner.com/blog/jetson-nano-yolo-object-detection


Algorithmen:

https://github.com/TheAlgorithms/C

https://www.algorithm-archive.org/

Anomaly Detection:

https://ff12.fastforwardlabs.com/

Objekte vermessen

https://github.com/mohammadst99/measureOBJECT


git:

https://www.vogella.com/tutorials/GitSubmodules/article.html


Step Up & use Tmux; The terminal multiplexer on steroids:

https://www.youtube.com/watch?v=RvsTIt7cjy0


Python-Tk and Matplotlib:

https://pythonprogramming.net/how-to-embed-matplotlib-graph-tkinter-gui/


GyrosGeier: .gitconfig -> https://paste.debian.net/plain/1255760

``[user]
``	name = Simon Richter
``   email = Simon.Richter@hogyros.de
``[init]
``       	defaultBranch = main
``[alias]
``       	graph = log --graph --all --oneline --decorate
``       	continuous-graph = "!watch -tc 'git -c color.ui=always graph | grep -A 60 -B 30 36mHEAD'"
``       	slowrebase = "!bash -c 'for i in $(git rev-list --reverse $(git merge-base HEAD @{u})..@{u}); do git rebase $i || break; done'"
``       	steprebase = "!bash -c 'git rebase $(git rev-list --reverse $(git merge-base HEAD @{u})..@{u} | head -1)'"
``[merge]
``       	tool = vimdiff


Vulkan-Stuff:

* viele Build-Files für alle möglichen Sachen ( Linux, Win, Mingw64, ... )
https://github.com/danilw/vulkan-shadertoy-launcher

* GLFW und Vulkan
https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Base_code#page_Integrating-GLFW


* Machine Learning
https://www.khronos.org/developers/linkto/vulkan-ml-japan-virtual-open-house-feb-2021

* Machine Leaerning mit Webcam-Sample und download der Toolchain und Onnx-Simplifier
https://github.com/Nebula4869/YOLOv5-ncnn-vulkan

* NCNN auf Jetson Nano
https://qengineering.eu/install-ncnn-on-jetson-nano.html

* Textur aus Framebuffer
https://github.com/Backseating-Committee-2k/vhdl

* Game Engine ( in C )
https://github.com/travisvroman/kohi





